<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\QuestionProController;

class ActualizarRegistros extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:registros';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el registro de entradas y salidas de las puertas electronicas, hacia el sistema GDTH.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new QuestionProController();
        $controller->inicio();
    }
}
