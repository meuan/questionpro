<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    protected $table = 'ciudades';
    protected $fillable =  [
        'id',
        'cliente_id',
        'ultimo_registro',
        'fecha',
        'usuario',
        'password',
        'direccion',
        'respuesta',
    ];
    public $timestamps = false;
}
