<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use DateTime;
use GuzzleHttp\Client;

class QuestionProController extends Controller
{
    protected $url;
    protected $cliente_id;
    protected $config_id;
    public function __construct(){
        $this->url = "";
    }

    public function inicio(){
        $ultimo = 0;
        $codigo = 403;
        $login = false;
        $response = false;
        $conf = DB::table("configuraciones")->take(1)->get();

        if(count($conf) > 0){
            $ultimo = $conf[0]->ultimo_registro;
            $this->cliente_id = $conf[0]->cliente_id;
            $this->config_id = $conf[0]->id;

            $registros = DB::connection('puertas')->table('acc_monitor_log')
                    ->where("id", ">", $ultimo)
                    ->select("id", "time as registro", "card_no as tarjeta")
                    ->orderBy("id","asc")->get();

            if(count($registros) > 0){
                $login = $this->login($conf);
                if($login == false){
                    $response = "No se logro realizar el login, favor de validar los datos";
                    dd($response);
                }
            }
            else{
                $response = "Actualizado";
                $codigo = 200;
            }
        }

        if($login != false){
            $response = $this->agrega_registros($login, $registros);
            $codigo = 200;
        }
        return response()->json($response, $codigo);;
    }

    private function login($config){
        $response = false;
        $login = false;
        $this->url = $config[0]->direccion."/api/";
        $usu = $config[0]->usuario;
        $pass = $this->desencriptar($config[0]->password);

        try{
            $cliente = new Client(['base_uri' => $this->url ]);
            $param = array( 'email' => $usu,'password' => $pass);
            $login = $cliente->request('POST', 'auth/login', array('form_params' => $param ));
            $res = json_decode($login->getBody()->getContents());
            $response = $res->access_token;
        }
        catch(Exception $ex){
            $response = false;
        }
       
        return $response;
    }

    private function agrega_registros($login, $registros){
        $code = 403;
        $messaje = "";
        try{
            $cliente = new Client(['base_uri' => $this->url ]);
            $param = array('cliente_id' => $this->cliente_id, 'registros' => json_encode($registros));
            $headers =  [ 'Authorization' => 'Bearer ' . $login,'Accept' => 'multipart/form-data' ];
            $response = $cliente->request('POST', 'registros-checadores/registros', array('form_params' => $param , 'headers' => $headers ));
            $ultimo = count($registros) - 1;

            DB::table("configuraciones")->where('id', $this->config_id)
                ->update([
                    'ultimo_registro' => $registros[$ultimo]->id,
                    'fecha' => new DateTime('now')]);
            $code = 200;
            $messaje = "Operacion exitosa";
        }
        catch(Exception $ex){
            dd("fallo");
            $messaje = $ex;
        }
        return response()->json($messaje, $code);
    }

    private static function desencriptar($string){
        $key=hash('sha256', 'QPPA55_$2020');
        $iv=substr(hash('sha256', '110790'), 0, 16);
        $output=openssl_decrypt(base64_decode($string), 'AES-256-CBC', $key, 0, $iv);
        return $output;
    }
}
