<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;
use Exceprtion;
class ConfiguracionController extends Controller
{
    
    public function index()
    {
        return view('welcome');
    }

   
    public function store(Request $request)
    {
        $mensaje = "F";
        $pass = $this->encriptar($request->get('password'));
        try{
            $res = DB::table("configuraciones")->where('id', 1)->update([
                'usuario' => $request->get('email'),
                'password' => $pass
            ]);
            if($res == true){
                $mensaje = "T";
            }
        }
        catch(Exception $ex){
            $mensaje = "F";
        }
        return back()
        ->withErrors(['mensaje' => $mensaje ]); 

        //return redirect('/')->with('Mensaje', $mensaje);
    }

    private static function encriptar($string){
        $output=FALSE;
        $key=hash('sha256', 'QPPA55_$2020');
        $iv=substr(hash('sha256', '110790'), 0, 16);
        $output=openssl_encrypt($string, 'AES-256-CBC', $key, 0, $iv);
        $output=base64_encode($output);
        return $output;
    }
    
}
