<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuraciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cliente_id');
            $table->bigInteger('ultimo_registro');
            $table->dateTime('fecha');
            $table->string('usuario');
            $table->string('password');
            $table->string('direccion');
            $table->string('respuesta',50);
        });

        DB::table('configuraciones')->insert(array(
            'id' => '1',
            'cliente_id' => 2,
            'ultimo_registro' => 167438,
            'fecha' => '2020-01-01',
            'usuario' => '',
            'password' => '',
            'direccion' => 'https://apigdth.pbmg.com.mx',
            'respuesta' => 'OK',
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuraciones');
    }
}
