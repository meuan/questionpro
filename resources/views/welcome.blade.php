<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Question Pro</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/sweetalert.js"></script>
        
        <style>
            .abs-center {
                display: flex;
                align-items: center;
                justify-content: center;
                min-height: 100vh;
            }

            .form {
                width: 450px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="abs-center">
                
                <form method="post" action="{{url('/configuracion')}}" class="border p-3 form" id="formID">
                    {{ csrf_field() }}
                    <h2 >Configuración Inicial</h2>
                    <div class="form-group">
                        <label for="email">Usuario:</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Usuario" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña:</label>
                        <input type="password" name="password" minlength="8" id="password" class="form-control" placeholder="Contraseña" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Guardar">
                    </div>
                    <input id="respuesta" type="hidden" value="{!!$errors->first('mensaje',':message')!!}">
                </form>
            </div>
        </div>
    </body>

    <script>
        $( document ).ready(function() {
            var res = $("#respuesta").val();
            console.log(res);
            if(res == "T"){
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Operacion realizada exitosamente.",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
            else if(res == "F"){
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Se presento un fallo favor de reportar al administrador.",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
</script>
</html>
